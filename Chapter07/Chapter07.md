# Chapter07 스테이트 패턴

## 7.1 스테이트 패턴

실세계의 많은 개체는 자신이 처한 상태에 따라 일을 다르게 수행한다. 이를 표현하는 가장 직접적이고 직관적인 방법은 상태 하나하나가 어떤 상태인지 검사해 일을 다르게 수행하게끔 하는 것이다. 하지만 이는 코드를 이해하거나 수정하기 어렵게 만든다.

이런 방식과는 달리 스테이트 패턴(State Pattern)은 어떤 행위를 할 때 상태에 행위를 수행하도록 위임한다. 이를 위해 스테이트 패턴에서는 **시스템의 각 상태를 클래스로 분리해 표현하고, 각 클래스에서 수행하는 행위들을 메서드로 구현**한다. 그리고 이러한 상태들을 **외부로부터 캡슐화하기 위해 인터페이스를 만들어 시스템의 각 상태를 나타내는 클래스로  실체화**하게 한다. 상태에 따라 행동이 변화하는 객체에 모두 적용할 수 있다.



![Alt text](스테이트패턴컬레보레이션.PNG)



- State : 시스템의 모든 상태에 공통 인터페이스를 제공.
- State1, State2, State3 : Context 객체가 요청한 작업을 자신의 방식으로 실제 실행. 상태 변경을 Context 객체에 요청하는 역할도 수행.
- Context : State를 이용하는 역할을 수행. 현재 시스템의 상태를 나타내는 상태 변수(state)와 실제 시스템의 상태를 나타내는 여러 가지 변수 존재.



**스테이트 패턴은 상태에 따라 동일한 작업이 다른 방식으로 실행될 때 해당 상태가 작업을 수행하도록 위임하는 디자인 패턴이다.**



<br/>

---

## 7.2 상태 머신 다이어그램

UML에서 상태와 상태 변화를 모델링하는 도구로 상태 머신 다이어그램(State Machine Diagram)이 있다.

아래의 다이어그램은 선풍기를 표현하는 상태 다이어그램이다.

![Alt text](선풍기상태머신다이어그램.PNG)

이 다이어그램에서는 모서리가 둥근 사각형으로 상태(state)를 나나태고, 상태 사이에 화살표를 사용해 상태 전이(state transition)을 나타낸다.



**상태** 

- 객체가 시스템에 존재하는 동안 객체가 가질 수 있는 어떤 조건이나 상황을 표현한다. ex) 어떤 액티비티 등을 수행하거나 특정 이벤트가 발생하기를 기다리는 것
- 의사상태(pseudo state)
  - 시작
  - 종료
  - 히스토리
  - 선택
  - 교차
  - 포크
  - 조인
  - 진입점
  - 진출점



위와 같은 내용을 통해 선풍기의 상태 머신 다이어그램을 아래와 같이 해석할 수 있다.

- 선풍기는 기본적으로 OFF 상태에서 시작한다.

- OFF 상태에서 사용자가 선풍기 스위치를 켜면 switch_on 이벤트를 발생시킨다. 이 때 전원이 들어온 상태라면(power_exits 조건) ON 상태로 진입하게 되고, 이때 turnon 액션을 실행하게 된다. 하지만 전원이 안들어온다면(power_exits 조건) OFF 상태에 머무른다.
- 사용자가 ON 상태에서 동작 버튼을 누르면 run 이벤트를 발생시키고 WORKING 상태로 진입한다. 이 때 operate 액션을 실행하게 된다.
- 선풍기가 ON 상태나 WORKING 상태에 머무를 때 사용자가 스위치를 끄면 switch_off 이벤트가 발생하고, 이 이벤트로 인해 OFF 상태로 진입한다.



<br/>

---

## 7.3 스테이트 패턴 예제 : 형광등 만들기

### 7.3.1 설명

형광등의 행위에 대해 분석한 내용은 다음과 같다.

- 형광등이 꺼져 있을 때 외부에서 ON 버튼을 누르면 형광등이 켜지고, 형광등이 켜져 있을 때 외부에서 OFF 버튼을 누르면 꺼진다.

- 이미 형광등이 켜져 있다면 ON 버튼을 누르면 형광등은 그대로 켜져 있고, 꺼져 있는 상태라면 OFF 버튼을 눌러도 변화가 없어야 한다.

- 형광등은 항상 처음에는 꺼져 있는 상태이어야 한다.

  

![Alt text](형광등상태머신다이어그램.PNG)



```java
public class Light {
    private static int ON = 0;  // 형광등 켜진 상태
    private static int OFF = 1; // 형광등 꺼진 상태
    private int state;          // 형광등 현재 상태

    public Light() { // 형광등 초기 상태는 꺼져 있는 상태임
        state = OFF;
    }

    public void on_button_pushed() {
        if (state == ON) {
            System.out.println("반응 없음");
        } else { // 형광등이 꺼져 있을 때 On 버튼을 누르면 켜진 상태로 전환
            System.out.println("Light On!");
            state = ON;
        }
    }

    public void off_button_pushed() {
        if (state == OFF) {
            System.out.println("반응 없음");
        } else { // 형광등이 켜져 있을 때 Off 버튼을 누르면 꺼진 상태로 전환
            System.out.println("Light Off!");
            state = OFF;
        }
    }
}

class Client {
    public static void main(String[] args) {
        Light light = new Light(); 
        light.off_button_pushed();  // 반응 없음
        light.on_button_pushed();   // 켜짐
        light.off_button_pushed();  // 꺼짐
    }
}

```



### 7.3.2 문제점

> 형광등에 새로운 상태를 추가할 때, 가령 형광등에 '취침등' 상태를 추가하려면?

- 형광등이 켜져 있을 때 ON 버튼을 누르면 원래 켜진 상태가 아닌 "취침등(SLEEPING)" 상태로 변경된다.
- 취침등 상태에 있을 때 ON 버튼을 누르면 형광등이 다시 켜지고 OFF 버튼을 누르면 꺼지게 된다.

![Alt text](취침등상태추가다이어그램.PNG)



```java
public class Light {
    private static int ON = 0;
    private static int OFF = 1;
    private static int SLEEPING = 2; // 형광등 취침모드 추가

    private int state;   // 초기 상태에는 형광등이 꺼져 있는 상태
    
    public Light() {
        state = OFF;
    }

    public void off_button_pushed() {
        if (state == OFF) {
            System.out.println("반응 없음");
        } else if (state == SLEEPING) { // 취침 상태 조건문 추가
            // 형광등이 취침 상태에 있는 경우 Off 버튼을 누르면 꺼진 상태로 전환
            System.out.println("Light Off!");
            state = OFF;
        } else {
            // 형광등이 켜져 있을 때 Off버튼을 누르면 꺼진상태로 전환
            System.out.println("Light Off!");
            state = OFF;
        }
    }
    
    public void on_button_pushed() {
        if (state == ON) { // 형광등이 켜져 있는 상태에서 취침 상태로 변경
            System.out.println("Sleeping!");
            state = SLEEPING;
        } else if (state == SLEEPING) { // 취침 상태 조건문 추가
            // 형광등이 취침 상태에 있는 경우 On 버튼을 누르면 켜진 상태로 전환
            System.out.println("Light On!");
            state = ON;
        } else {
            // 형광등이 꺼져 있을 때 On버튼을 누르면 켜진상태로 전환
            System.out.println("Light On!");
            state = ON;
        }
    }
}
```



상태 진입이 복잡한 조건문에 내포된 지금의 코드 구조는 현재 시스템의 상태 변화를 파악하기에 용이하지 않다. 그리고 새로운 상태가 추가되는 경우에 이를 반영하기 위해 코드를 수정해야만 한다.



### 7.3.3 해결책

스테이트 패턴을 적용하여 해결하기 위해서는 **변하는 부분을 찾아 캡슐화**하는 것이 매우 중요하다. 또한 현재 시스템이 **어떤 상태에 있는지와 상관없게 구성**하고, **상태 변화에도 독립적**으로 코드를 수정해야 한다.

![Alt text](패턴적용형광등다이어그램.PNG)

스트래티지 패턴과 구조가 매우 흡사하다. Light 클래스에서 구체적인 상태 클래스가 아닌 추상화된 State 인퍼테이스만 참조하므로 현재 어떤 상태에 있는지와 무관하게 코드를 작성할 수 있다. Light 클래스에서는 상태 클래스에 작업을 위임만 하면 된다.



```java
// 상태 인터페이스
public interface State {
    public void on_button_pushed(Light light);
    public void off_button_pushed(Light light);
}
```

```java
public class Light {
    private State state;

    public Light(){
        state = OFF.getInstance();
    }

    public void setState(State state){
        this.state = state;
    }

    public void on_button_pushed(){
        state.on_button_pushed(this);
    }

    public void off_button_pushed(){
        state.off_button_pushed(this);
    }
}
```

```java
public class ON implements State{
    private static ON on = new ON();
    private ON(){}

    public static ON getInstance(){
        return on;
    }

    @Override
    public void off_button_pushed(Light light) {
        System.out.println("반응 없음");
    }

    @Override
    public void on_button_pushed(Light light) {
        light.setState(OFF.getInstance());
        System.out.println("Light Off!");
    }
}
```

```java
public class OFF implements State {
    private static OFF off = new OFF();
    private OFF(){}

    public static OFF getInstance(){ // 초기화된 OFF 클래스의 인스턴스 반환
        return off;
    }
    @Override
    public void off_button_pushed(Light light) { // OFF 상태일 때 ON 누르면 ON 상태임
        light.setState(ON.getInstance());
        System.out.println("Light On!!");
    }

    @Override
    public void on_button_pushed(Light light) { // OFF 상태일 때 OFF 누르면 변화 없음
        System.out.println("반응 없음");
    }
}

```

```java
public class Client {
    public static void main(String[] args) {
        Light light = new Light();

        light.off_button_pushed();  // 반응 없음
        light.on_button_pushed();   // 불켜짐
        light.on_button_pushed();   // 반응 없음
        light.off_button_pushed();  // 불꺼짐
    }
}
```

