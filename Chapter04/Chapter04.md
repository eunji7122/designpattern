# 디자인 패턴

<br/>

## 4.1 디자인 패턴의 이해

**디자인 패턴**
- 경험이 맣은 소프트웨어 엔지니어들이 소프트웨어를 설계할 때 자주 발생하는 고질적인 문제들의 해법을 모아놓은 것

**디자인 패턴 구조**
- 콘텍스트 : 문제가 발생하는 여러 상황을 기술
- 문제 : 패턴이 적용되어 해결될 필요가 있는 디자인 이슈
- 해결 : 문제를 해결하도록 설계를 구성하는 요소들과 그 요소들 사이의 관계, 책임, 협력 관계를 기술

*예시*
- 콘텍스트 : 클래스가 객체 생성 과정을 제어해야 하는 상황
- 문제 : 어플리케이션이 전역적으로 접근하고 관리할 필요가 있는 데이터를 포함한다. 동시에 이러한 데이터는 시스템에 유일하다. 어떤 방식으로 클래스에서 생성되는 객체의 수를 제어하고 클래스의 인터페이스에 접근하는 것을 제어해야 하는가?
- 해결 : 클래스의 생성자를 public으로 정의하지 말고 private이나 protected로 선언해 외부에서 생성자를 이용해 객체를 일단 생성할 수 없게 만들고 ...

-> '싱글톤(singleton) 패턴'

<br/>
<br/>

---

## 4.2 GoF 디자인 패턴

GoF(Gang of Four)는 4명의 개발자들이 디자인 패턴을 23가지로 정리하고, 생성, 구조, 행위의 3가지로 분류

![Alt text](디자인패턴의분류.PNG)

- 생성 패턴 : 객체 생성에 관련된 패턴으로, 객체의 생성과 조합을 캡슐화해 특정 객체가 생성되거나 변경되어도 프로그램 구조에 영향을 크게 받지 않도록 유연성을 제공
- 구조 패턴 : 클래스나 객체를 조합해 더 큰 구조를 만드는 패턴
- 행위 패턴 : 객체나 클래스 사이의 알고리즘이나 책임 분배에 관련된 패턴

<br/>
<br/>

---

## 4.3 UML과 디자인 패턴

**컬레보레이션**

컬레보레이션 : UML(통합 모델링 언어) 2.0에서 디자인 패턴을 표현하는 도구로 디자인 패턴을 정확하게 표현하려면 구조적인 면과 행위적인 면을 모두 표현
- 구조적인 면 : 어떤 요소들이 주어진 목적을 달성하기 위해 협력하는지
- 행위적인 면 : 협력을 위한 요소들의 상호작용


![Alt text](컬레보레이션.PNG)

- 역할들의 상호작용을 추상화한 것
- UML에서 컬레보레이션은 점선으로 된 타원 기호를 사용
- 타원 내부에 협력을 필요로 하는 역할들과 그들 사이의 연결 관계를 표현

![Alt text](어커런스.PNG)

- 더 구체적인 상황에서의 컬레보레이션 적용을 표현


<br/>

**순차 다이어그램**

순차 다이어그램 : UML 2.0에서 객체들의 상호작용을 나타내는 다이어그램 중 하나
- 기본적으로 하나의 시나리오에 관한 객체 사이의 상호작용을 보여주는데 사용

![Alt text](순차다이어그램.PNG)

- 객체는 가장 윗부분에 표현, 왼쪽에서 오른쪽으로 나열
- 생명선 : 객체가 존재함을 보여줌
- 활성 구간 : 객체가 연산을 실행하는 상태임을 보여줌
- 메시지
  - 비동기 메시지 : 머리 부분이 열려있는 화살표로, 메시지를 송신한 후 메시지 실행이 끝나기를 기다리지 않고 다음 작업 수행 가능
  - 동기 메시지 : 머리 부분이 채워진 화살표로, 메시지의 실행이 종료될 때까지 다음 작업을 수행 불가
- 가드 : 메시지가 송신되는 데 만족해야하는 조건

메시지를 표현할 때는 다음과 같은 형식을 따른다.
```
[시퀀스 번호][가드] : 반환 값 := 메시지 이름([인자 리스트])
```
- 메시지 이름을 제외하고 모두 생략 가능

프레임은 복잡한 부분을 간략화 해놓고 다른 곳에 펼쳐 보이거나 특별한 기능을 하는 것 -> alt

![Alt text](alt.PNG)


<br/>

**순차 다이어그램과 클래스 다이어그램의 관계**

순차 다이어그램 : 객체 사이의 메시지 흐름과 순서를 알려주는 행위 측면에 중점을 두는 모델

클래스 다이어 그램 : 시스템의 구조적인 측면에 중점을 두는 모델

-> 시스템을 모델링할 때 서로 정합이 이루어져야 함.