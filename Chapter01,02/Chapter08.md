# Chapter08 추상 클래스(Abstract Class)

## 8.1 추상 클래스의 개념

슈퍼클래스에 정의된 메소드가 서브클래스들의 공통 기능을 나타내도록 구현하는 것이 어려운 경우가 있다. 이를 위해 추상 클래스라는 개념을 활용한다.

**두가지 수준의 기능 공통성**

메소드가 메소드 시그니처와 메소드 바디로 구성되기 때문에, 메소드 시그니처 수준의 공통성과 메소드 바디 수준의 공통성으로 분류할 수 있다.

- 명세 수준의 공통성
  - 여러 클래스들이 기능 명세만 동일하게 필요(바디 구현 부분에서는 공통성이 없음)

- 구현 수준의 공통성
  - 여러 클래스들이 기능 명세뿐 아니라 구현 부분까지 동일하게 필요

![Alt text](추상클래스.PNG)

<br/>

**추상 메소드와 추상 클래스**

추상 메소드
- 시그니처만 정의되어 있고, 구현체가 없는 메소드 -> 명세 수준 공통성
  
추상 클래스
- 가상 클래스라고도 하며, 추상 메소드를 하나 이상 가지고 있는 슈퍼클래스의 한 형태 -> 명세 수준 공통성, 기능 수준의 공통성
- 객체 생성이 불가 -> 일부가 완전하게 구현되지 않은 상태로 정의
- 서브클래스가 반드시 재정의하는 기능에 대한 명세를 정의

![Alt text](추상클래스와구체적클래스.PNG)

- shape 클래스의 모든 서브클래스들은 추상 메소드로 선언된 2개 메소드를 각자 특성에 맞게 내부 구현을 작성하여 Concrete 클래스로 정의

<br/>

**추상 클래스의 장점**

1. 프로그램 구조에 대한 이해도 향상
2. 재사용성 향상
3. 확장성 향상
4. 유지보수성 향상

<br/>
<br/>

## 8.2 객체지향 언어에서의 추상 메소드와 추상 클래스

![Alt text](도형클래스.PNG)

<br/>

**추상 메소드 정의**

```java
abstract [반환타입] [메소드명]([입력 매개변수 목록]);

public abstract void move();
```

<br/>

**추상 클래스와 Concreate 클래스 정의**

```java
public abstract class Shape {
    private String color;
    
    public Shape(String aColor){
        color = aColor;
    }
    
    public String getColor(){
        return color;
    }
    
    // 2개의 추상 메소드 선언
    public abstract void move(int xx, int yy);
    public abstract void draw();
}
```

```java
public class Circle extends Shape {
    private int centerX, centerY;
    private float radius;

    public Circle(int x, int y, float r, String aColor){
        super(aColor);
        centerX = x;
        centerY = y;
        radius = r;
    }
    
    @Override
    public void move(int xx, int yy) {
        centerX += xx;
        centerY += yy;
    }

    @Override
    public void draw() {
        System.out.println("====== Circle Information ======");
        System.out.println("Center: (" + centerX + ", " + centerY + ")");
        System.out.println("Radius: " + radius);
        System.out.println("Color: " + getColor());
    }
}
```

<br/>
<br/>

## 8.3 추상 클래스의 활용

1. 복수 개의 Concreate 클래스 간에 공통적인 속성과 메소드는 추상 클래스에 위치키시고, Concreate 클래스 간에 기능성 명세만 동이랗고 그 구현 방식이 다른 메소드는 추상 메소드로 정의한다.
2. 각 Concreate 클래스는 추상 클래스를 상속하고, 모든 추상 클래스를 오버라이드하여 각 클래스의 목적에 맞게 재정의한다.
3. 객체 대치성을 활용하여 추상 클래스 타입의 자료 구조를 정의하고, 자료 구조의 각 요소는 Concreate 클래스 인스턴스를 가리키도록 한다.
4. 동적 바인딩을 활용하여 자료 구조의 각 요소를 슈퍼클래스인 추상 클래스 타입의 변수로 접근하도록 하면, 실제 가리키고 있는 인스턴스 타입에 따라 오버라이딩된 메소드가 실행된다.


```java
public class Main {
    public static void main(String[] args) {
        Shape shapes[] = new Shape[4];

        shapes[0] = new Circle(5, 5, 5.4f, "Red");
        shapes[1] = new Circle(7, 9, 6.9f, "Blue");
        shapes[2] = new Square(4, 7, 3, "Yellow");
        shapes[3] = new Triangle(1, 2, 5, 6, 10, 12, "Green");

        for(int i = 0; i < shapes.length; i++){
            shapes[i].draw();
        }
    }
}
```

단순히 상속 메커니즘을 활용하여 슈퍼클래스 밑 서브클래스를 활용하는 경우와 달리, 추상클래스는 다음과 같은 이점이 있다.
- 슈퍼클래스는 모든 메소드가 반드시 구현체를 가져야 하는데, 서브클래스 간의 공통 구현체를 찾기가 어려울 때가 있음
- 각 클래스마다 목적에 맞게 구현된 메소드를 호출 가능