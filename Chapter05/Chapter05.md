# Chapter05 스트래티지 패턴

스트래티지 패턴(Strategy Pattern)은 구현한 클래스의 알고리즘이 변경될 경우, 모든 클래스를 바꿔야하는 어려움을 해결하기 위한 패턴으로 클래스의 행동을 메서드화하여 변화에 유연한 캡슐화 처리를 해준다.

## 5.1 로봇 만들기

![Alt text](로봇추가.PNG)

현재 위 그림처럼 캡슐화 단위에 따라 새로운 로봇을 추가하기는 매우 쉽다. 그러나 새로운 로봇에 기존의 공격 또는 이동 방법을 추가하거나 변경하려고 하면 문제가 발생한다.

만약 Sungard에 TeakwonV의 미사일 공격 기능을 사용하려고 하면 TaekwonV와 Sungard의 attack 메서드가 중복된다. 이러한 구조는 OCP 법칙 위반 뿐만 아니라 코드의 중복으로 나중에 심각한 문제를 일으킨다.

로봇 설계에서의 문제를 해결하려면 **무엇이 변화되었는지** 찾아야 한다. 로봇 예제에서 문제는 로봇의 **이동 방식과 공격 방식의 변화**다. 그러므로 이 두 속성을 **인터페이스로 만들어 캡슐화**한다. 

![Alt text](기능캡슐화.PNG)

구체적인 이동 방식과 공격 방식이 MovingStratgy와 AttackStrategy 인터페이스에 의해 캡슐화되어 있다. 새로운 기능의 추가가 기존의 코드에 영향을 미치지 못하게 하므로 OCP를 만족하는 설계가 된다.

이렇게 변경된 새로운 구조에서는 외부에서 로봇 객체의 이동 방식과 공격 방식을 임의대로 바꾸기 위해 Robot 클래스에 setMovingStrategy와 setAttackStratgy 메서드를 정의했다. 이런 변경이 가능한 이유는 상속 대신 집약 관계를 이용했기 때문이다.

```java
public class Robot {
    private String name;
    private MovingStrategy movingStrategy;
    private AttackStrategy attackStrategy;

    public Robot(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void move(){
        movingStrategy.move();
    }

    public void attck(){
        attackStrategy.attack();
    }

    public void setMovingStrategy(MovingStrategy movingStrategy){
        this.movingStrategy = movingStrategy;
    }

    public void setAttackStrategy(AttackStrategy attackStrategy){
        this.attackStrategy = attackStrategy;
    }
}

class Atom extends Robot{
    public Atom(String name){
        super(name);
    }
}

class TaekwonV extends Robot{
    public TaekwonV(String name){
        super(name);
    }
}

interface MovingStrategy{
    public void move();
}

class FlyingStrategy implements MovingStrategy{
    public void move(){
        System.out.println("I can fly.");
    }
}

class WalkingStrategy implements MovingStrategy{
    public void move(){
        System.out.println("I can only walk.");
    }
}

interface AttackStrategy{
    public void attack();
}

class MissileStrategy implements AttackStrategy{
    public void attack(){
        System.out.println("I have Missile and can attack with it.");
    }
}

class PunchStrategy implements AttackStrategy{
    public void attack(){
        System.out.println("I have strong punch and can attack with it.");
    }
}

class Client{
    public static void main(String[] args) {
        Robot taekwonV = new TaekwonV("TaekwonV");
        Robot atom = new Atom("Atom");

        taekwonV.setMovingStrategy(new WalkingStrategy());
        taekwonV.setAttackStrategy(new MissileStrategy());

        atom.setMovingStrategy(new FlyingStrategy());
        atom.setAttackStrategy(new MissileStrategy());

        System.out.println("My name is " + taekwonV.getName());
        taekwonV.move();
        taekwonV.attck();

        System.out.println();

        System.out.println("MY name is " + atom.getName());
        atom.move();
        atom.attck();
    }
}
```

<br/>

---

## 5.2 스트래티지 패턴

스트래티지 패턴은 같은 문제를 해결하는 여러 알고리즘이 클래스별로 캡슐화되어 있고 이들이 필요할 때 교체할 수 있도록 함으로써 동일한 문제를 다른 알고리즘으로 해결할 수 있게 하는 디자인 패턴이다.

![Alt text](컬레보레이션.PNG)

![Alt text](순차다이어그램.PNG)

![Alt text](컬레보레이션.PNG)